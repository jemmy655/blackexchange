package com.czbank.blackexchange.model;

public class School {
    private Integer id;

    private String schoolName;

    private String schoolDebitCard;

    private String country;
    
    private Integer currencyId;

    public School() {
		super();
		// TODO Auto-generated constructor stub
	}

	public School(String schoolName, String schoolDebitCard, String country, Integer currencyId) {
		super();
		this.schoolName = schoolName;
		this.schoolDebitCard = schoolDebitCard;
		this.country = country;
		this.currencyId = currencyId;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolDebitCard() {
        return schoolDebitCard;
    }

    public void setSchoolDebitCard(String schoolDebitCard) {
        this.schoolDebitCard = schoolDebitCard;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}