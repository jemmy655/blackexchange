package com.czbank.blackexchange.model;

public class BankUser {
    private Integer id;

    private String name;

    private String sex;

    private Integer age;

    private String identityCard;

    private String debitCard;

    private String password;
    
    private String username;
    
    private String tel;
    
    public BankUser() {
		super();
	}

	public BankUser(String name, String sex, Integer age, String identityCard, String debitCard, String password,
			String username, String tel) {
		super();
		this.name = name;
		this.sex = sex;
		this.age = age;
		this.identityCard = identityCard;
		this.debitCard = debitCard;
		this.password = password;
		this.username = username;
		this.tel = tel;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getDebitCard() {
        return debitCard;
    }

    public void setDebitCard(String debitCard) {
        this.debitCard = debitCard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}