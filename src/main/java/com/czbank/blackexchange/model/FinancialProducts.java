package com.czbank.blackexchange.model;

public class FinancialProducts {
    private Integer id;

    private String financialProductName;

    private Integer currencyId;

    private Double rateReturn;
    
    private String description;
    
    private String type;
    
    public FinancialProducts() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FinancialProducts(String financialProductName, Integer currencyId, Double rateReturn, String description,
			String type) {
		super();
		this.financialProductName = financialProductName;
		this.currencyId = currencyId;
		this.rateReturn = rateReturn;
		this.description = description;
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFinancialProductName() {
        return financialProductName;
    }

    public void setFinancialProductName(String financialProductName) {
        this.financialProductName = financialProductName;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public Double getRateReturn() {
        return rateReturn;
    }

    public void setRateReturn(Double rateReturn) {
        this.rateReturn = rateReturn;
    }
}