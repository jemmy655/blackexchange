package com.czbank.blackexchange.model;

import java.util.Date;

public class ExchangeRateRecord {
    private Integer id;

    private Double exchangeRate;

    private Integer currencyId;

    private Date time;

    public ExchangeRateRecord(Double exchangeRate, Integer currencyId, Date time) {
		super();
		this.exchangeRate = exchangeRate;
		this.currencyId = currencyId;
		this.time = time;
	}

	public ExchangeRateRecord() {
		super();
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrency(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}