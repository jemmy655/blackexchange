package com.czbank.blackexchange.model;

public class Currency {
    private Integer id;

    private String currency;

    
    public Currency() {
		super();
	}

	public Currency(String currency) {
		super();
		this.currency = currency;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}