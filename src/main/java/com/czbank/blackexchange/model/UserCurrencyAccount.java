package com.czbank.blackexchange.model;

public class UserCurrencyAccount {
    private Integer id;

    private Integer currencyId;

    private Double amount;

    private Integer userId;

    public Integer getId() {
        return id;
    }

    public UserCurrencyAccount() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserCurrencyAccount(Integer currencyId, Double amount, Integer userId) {
		super();
		this.currencyId = currencyId;
		this.amount = amount;
		this.userId = userId;
	}

	public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}