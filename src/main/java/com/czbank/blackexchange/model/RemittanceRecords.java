package com.czbank.blackexchange.model;

import java.util.Date;

public class RemittanceRecords {
    private Integer id;

    private String receiverAccount;

    private String paymentAccount;

    private Double amount;

    private Date time;

    private String note;

    private Integer userId;
    
    private String status;

    public RemittanceRecords() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RemittanceRecords(String receiverAccount, String paymentAccount, Double amount, Date time, String note,
			Integer userId, String status) {
		super();
		this.receiverAccount = receiverAccount;
		this.paymentAccount = paymentAccount;
		this.amount = amount;
		this.time = time;
		this.note = note;
		this.userId = userId;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public String getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(String paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}