package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.UserRole;
import java.util.List;

public interface UserRoleMapper {
    int insert(UserRole record);

    List<UserRole> selectAll();
}