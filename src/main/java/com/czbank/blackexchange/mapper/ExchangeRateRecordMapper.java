package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.ExchangeRateRecord;
import java.util.List;

public interface ExchangeRateRecordMapper {
    int insert(ExchangeRateRecord record);

    List<ExchangeRateRecord> selectAll();
}