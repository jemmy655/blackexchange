package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.Currency;
import java.util.List;

public interface CurrencyMapper {
    int insert(Currency record);

    List<Currency> selectAll();
}