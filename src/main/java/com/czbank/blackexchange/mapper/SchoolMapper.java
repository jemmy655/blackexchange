package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.School;
import java.util.List;

public interface SchoolMapper {
    int insert(School record);

    List<School> selectAll();
}