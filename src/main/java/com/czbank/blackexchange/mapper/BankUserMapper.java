package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.BankUser;
import java.util.List;

public interface BankUserMapper {
    int insert(BankUser record);

    List<BankUser> selectAll();
}