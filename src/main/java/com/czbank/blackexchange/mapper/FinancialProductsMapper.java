package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.FinancialProducts;
import java.util.List;

public interface FinancialProductsMapper {
    int insert(FinancialProducts record);

    List<FinancialProducts> selectAll();
}