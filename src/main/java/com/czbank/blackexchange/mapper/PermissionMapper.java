package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.Permission;
import java.util.List;

public interface PermissionMapper {
    int insert(Permission record);

    List<Permission> selectAll();
}