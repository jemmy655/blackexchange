package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.RemittanceRecords;
import java.util.List;

public interface RemittanceRecordsMapper {
    int insert(RemittanceRecords record);

    List<RemittanceRecords> selectAll();
}