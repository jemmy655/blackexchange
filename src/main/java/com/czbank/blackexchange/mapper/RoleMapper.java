package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.Role;
import java.util.List;

public interface RoleMapper {
    int insert(Role record);

    List<Role> selectAll();
}