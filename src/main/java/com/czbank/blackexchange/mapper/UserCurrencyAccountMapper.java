package com.czbank.blackexchange.mapper;

import com.czbank.blackexchange.model.UserCurrencyAccount;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface UserCurrencyAccountMapper {
	int insert(UserCurrencyAccount record);

	List<UserCurrencyAccount> selectAll();

	List<UserCurrencyAccount> listByUserId(Integer userId);
	

    @Delete("delete from user_currency_account where id = #{id}")
    public int delete(Integer id);
    //2update
    @Update("update user_currency_account set currency_id = #{currencyId} , amount = #{amount} , user_id = #{userId} where id = #{id}")
    public int update(UserCurrencyAccount uca);
    
    @Select("select * from user_currency_account where id=#{id} limit 1")
    public UserCurrencyAccount selectById(Integer id);
}