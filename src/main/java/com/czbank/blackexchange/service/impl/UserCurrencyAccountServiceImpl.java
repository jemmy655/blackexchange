package com.czbank.blackexchange.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.czbank.blackexchange.mapper.UserCurrencyAccountMapper;
import com.czbank.blackexchange.model.UserCurrencyAccount;
import com.czbank.blackexchange.service.UserCurrencyAccountService;

@Service
public class UserCurrencyAccountServiceImpl implements UserCurrencyAccountService {

	@Autowired
	UserCurrencyAccountMapper UCAMapper;

	@Override
	public int insert(UserCurrencyAccount record) {
		// TODO Auto-generated method stub
		return UCAMapper.insert(record);
	}

	@Override
	public List<UserCurrencyAccount> selectAll() {
		// TODO Auto-generated method stub
		return UCAMapper.selectAll();
	}

	@Override
	public List<UserCurrencyAccount> listByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return UCAMapper.listByUserId(userId);
	}

	@Override
	public int delete(Integer id) {
		return UCAMapper.delete(id);
	}

	@Override
	public int update(UserCurrencyAccount uca) {
		return UCAMapper.update(uca);
	}

	@Override
	public UserCurrencyAccount selectById(Integer id) {
		// TODO Auto-generated method stub
		return UCAMapper.selectById(id);
	}

}
