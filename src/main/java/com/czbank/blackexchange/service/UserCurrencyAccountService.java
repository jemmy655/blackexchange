package com.czbank.blackexchange.service;

import java.util.List;

import com.czbank.blackexchange.model.UserCurrencyAccount;

public interface UserCurrencyAccountService {

	int insert(UserCurrencyAccount record);

	List<UserCurrencyAccount> selectAll();

	List<UserCurrencyAccount> listByUserId(Integer userId);

	public int delete(Integer id);

	public int update(UserCurrencyAccount uca);

	public UserCurrencyAccount selectById(Integer id);
}
