package com.czbank.blackexchange;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.czbank.blackexchange.mapper")
public class BlackexchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlackexchangeApplication.class, args);
	}

}
