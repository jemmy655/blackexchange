package com.czbank.blackexchange.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.czbank.blackexchange.model.ExchangeRateRecord;
import com.czbank.blackexchange.model.UserCurrencyAccount;
import com.czbank.blackexchange.service.UserCurrencyAccountService;

@Controller
public class UserCurrencyAccountController {
	@Autowired
	UserCurrencyAccountService UCAService;

	@RequestMapping("/usercurrencyaccount/listbyuserid")
	@ResponseBody
	public List<UserCurrencyAccount> listbyUserId(Integer userId) {

		return UCAService.listByUserId(userId);
	}

	@RequestMapping("/usercurrencyaccount/total")
	@ResponseBody
	public Double total(Integer userId, Integer currencyId) {

		Double total = 0.0;
		Double rate = 0.0;
		List<UserCurrencyAccount> UCAList = UCAService.listByUserId(userId);
		List<ExchangeRateRecord> ERRList = new ArrayList<ExchangeRateRecord>();
		ExchangeRateRecord e1 = new ExchangeRateRecord();
		e1.setCurrency(1);
		e1.setExchangeRate(2.0);
		ExchangeRateRecord e2 = new ExchangeRateRecord();
		e2.setCurrency(2);
		e2.setExchangeRate(4.0);
		ExchangeRateRecord e3 = new ExchangeRateRecord();
		e3.setCurrency(3);
		e3.setExchangeRate(8.0);
		ERRList.add(e1);
		ERRList.add(e2);
		ERRList.add(e3);
		for (UserCurrencyAccount u : UCAList) {
			for (ExchangeRateRecord e : ERRList) {
				if (u.getCurrencyId().equals(e.getCurrencyId())) {
					total += (u.getAmount() * Double.valueOf(e.getExchangeRate()));
				}
				if (e.getCurrencyId().equals(currencyId))
					rate = Double.valueOf(e.getExchangeRate());
			}
		}
		if (0 == currencyId)
			return total;
		else
			return total / rate;

	}

	@RequestMapping("/usercurrencyaccount/delete")
	@ResponseBody
	public int deleteByUserId(Integer id) {

		return UCAService.delete(id);
	}

	@RequestMapping("/usercurrencyaccount/update")
	@ResponseBody
	public int update(UserCurrencyAccount uca) {

		return UCAService.update(uca);
	}

	@RequestMapping("/usercurrencyaccount/insert")
	@ResponseBody
	public int insert(UserCurrencyAccount uca) {
		System.out.println(uca);
		return UCAService.insert(uca);
	}
}
